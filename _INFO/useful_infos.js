
// MUST KNOW!
// CONSIST SECTION OF "PRIVATE ROUTES" and "PUBLIC ROUTES"
// SEARCH FOR THE SECTION FOR EASY NAVIGATION ON SERVICES AND VALIDATION FILES.


// let data = req.body; // Can be in JSON or Form Data or XML
// let params = req.params; // params is inlined with the url separated by "/id"
// let query = req.query; // query string line "id=1&name=erwinvicarme"


// FOR THE CODING STYLE (NAMING CONVENTION PLEASE REFER TO THIS LINK BELOW)
// https://basarat.gitbook.io/typescript/styleguide
// consider snake_head case also for variables


// this for loop uses are the property values
// for(let rounder of rounders){
// 	console.log(rounder) //output dog,cat,mouse,tiger,ele
// }
// this for loop uses index/keys
// for(let rounder in rounders){
// 	console.log(rounder) //output 0,1,2,3,4
// }

// async.eachSeries(Object.keys(TABLES_NAMES_CSV), function(keyValue, callback) {
// async.eachSeries(notifications, (notification, callback) => {
//  callback()
// }, (loopErr, loopRes) => {
//  code here if looping ended
// })


// HOW TO GENERATE SECRET-KEY?
//     https ://www.allkeysgenerator.com/Random/Security-Encryption-Key-Generator.aspx


// ALL VALIDATION
// https://sequelize.org/master/manual/validations-and-constraints.html

// SEQUELIZE DEADLOCK
// https://dev.to/anonyma/how-to-retry-transactions-in-sequelize-5h5c


// https://nodejs.org/en/download/releases/
// https://gist.github.com/LayZeeDK/c822cc812f75bb07b7c55d07ba2719b3