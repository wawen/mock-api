$ npm outdated
Package                Current  Wanted  Latest  Location
@google-cloud/storage   5.11.0  5.13.0  5.13.0  portal-api
async                    3.2.0   3.2.1   3.2.1  portal-api
chalk                    4.1.1   4.1.2   4.1.2  portal-api
eslint                  7.30.0  7.32.0  7.32.0  portal-api
eslint-plugin-import    2.23.4  2.24.0  2.24.0  portal-api
express-validator       6.12.0  6.12.1  6.12.1  portal-api
multer                   1.4.2   1.4.3   1.4.3  portal-api
mysql2                   2.2.5   2.3.0   2.3.0  portal-api
nodemailer               6.6.2   6.6.3   6.6.3  portal-api


also update sequelize into 6.5.5