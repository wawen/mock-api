# WHAT DOES THIS PROJECT CONTAIN?
REST API
NodeJS
express
sequelize
mysql2

## DATABASE CONFIG
Config is inside /config folder

## Cross-Origin Resource Sharing
Config is insde src/cors for middleware and responseHandler
mail is not yet in use.

## MODEL
Resources is insde /models folder.
Every time you add new model don't forget to declare it on src/models/index.js

## ROUTES AND CONTROLLERS
Resources are inside src/routes-handler
Routes and Controller are inside each management designated folder.

## SERVICES
This folder holds all the controllers function separately for each controller.

## VALIDATION
This holds all custom express validation for the request.

## SETUP
**npm install**

# RUN IN CROS ENV
Uses cross env to run ex: <br>
**npm run dev** <br>
**npm run staging** <br>
**npm run pruction** <br>
