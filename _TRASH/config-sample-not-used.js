module.exports = {
  apps: [{
    name: 'G-Retail Field App',
    script: "./src/server.js",
    instances: 1,
    watch: false,
    env_dev: {
      NODE_ENV: 'developments'
    },
    env_staging: {
      NODE_ENV: 'stagings'
    },
    env_production: {
      NODE_ENV: 'productions'
    }
  }],
};
