const config = require('config').get(mode);
const dbConfig = config.database;

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.database, dbConfig.user, dbConfig.password, {
  host: dbConfig.host,
  dialect: dbConfig.dialect,
  operatorsAliases: dbConfig.operatorsAliases,
  logging: dbConfig.logging,
  define: {
    timestamps: dbConfig.timestamps,
    // freezeTableName: dbConfig.freezeTableName
  },
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  },
  // This is to fix the deadlock promlem
  // "SequelizeDatabaseError: Deadlock found when trying to get lock; try restarting transaction"
  retry: {
    match: [
      Sequelize.ConnectionError,
      Sequelize.ConnectionTimedOutError,
      Sequelize.TimeoutError,
      /Deadlock/i,
      'SQLITE_BUSY'],
    max: 3
  }
});

const db = {};

// contains the require("sequelize");
// can be use to declare insted of import
// use in data type ex. Sequelize.Integer
db.Sequelize = Sequelize;

// holds the sequelize configs and pool
// can also be use for raw query
db.sequelize = sequelize;

db.user = require("./user.model.js")(sequelize, Sequelize);

module.exports = db;
