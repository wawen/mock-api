module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("user", {
    id: {
      // unique: true,
      //000,000,000,001
      type: Sequelize.INTEGER(12).UNSIGNED.ZEROFILL,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    username: {
      unique: true,
      type: Sequelize.STRING,
      allowNull: false
    },
    email_address: {
      unique: true,
      type: Sequelize.STRING,
      allowNull: false
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    dob: {
      type: Sequelize.DATEONLY,
      allowNull: true,
      defaultValue: null
    },
    age: {
      type: Sequelize.INTEGER(3),
      allowNull: true,
      defaultValue: null
    },
    gender: {
      type: Sequelize.STRING(64),
      allowNull: false,
      defaultValue: 'human'
    },
    nationality: {
      type: Sequelize.STRING(64),
      allowNull: true,
      defaultValue: null
    },
    user_role: {
      type: Sequelize.STRING(32),
      allowNull: false,
      defaultValue: 'user'
    },
    status: {
      type: Sequelize.STRING(32),
      allowNull: false,
      defaultValue: 'active'
    },
    status_account: {
      type: Sequelize.STRING(32),
      allowNull: false,
      defaultValue: 'unlocked'
    },
    status_disabled: {
      type: Sequelize.STRING(32),
      allowNull: false,
      defaultValue: 'no'
    },
    status_verification: {
      type: Sequelize.STRING(32),
      allowNull: false,
      defaultValue: 'unverified'
    },
    status_disabled_expiry: {
      type: Sequelize.STRING(32),
      allowNull: false,
      defaultValue: '24'
    },
    password_reset_url: {
      type: Sequelize.STRING(),
      allowNull: true,
      defaultValue: null
    },
    password_reset_code: {
      type: Sequelize.STRING(32),
      allowNull: true,
      defaultValue: null
    },
    password_attempt_count: {
      type: Sequelize.BOOLEAN,
      allowNull: true,
      defaultValue: null
    },
    image_url: {
      type: Sequelize.TEXT,
      allowNull: true,
      defaultValue: null
    },
    created_by: {
      type: Sequelize.INTEGER(12),
      allowNull: true,
      defaultValue: null
    },
    created_date: {
      // type: 'TIMESTAMP',
      type: 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
      allowNull: false,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
    },
    updated_by: {
      type: Sequelize.INTEGER(12),
      allowNull: true,
      defaultValue: null
    },
    updated_date: {
      // type: 'TIMESTAMP',
      type: 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      // onUpdate: Sequelize.literal('CURRENT_TIMESTAMP'),
    }
  }, {
    freezeTableName: true
  });
  // User.removeAttribute('id');
  return User;
};
