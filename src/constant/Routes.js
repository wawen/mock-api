module.exports = {
  USER_PUBLIC: '/v1/public/users',
  USER_PRIVATE: '/v1/private/users',
  COVEREDSTOREMASTER_PUBLIC: '/v1/public/covered-store-master',
  COVEREDSTOREMASTER_PRIVATE: '/v1/private/covered-store-master',
  GET_ERROR: '/v1/errors'
}
