drop database `portal`;
create database `portal`;
use `portal`;

-- DROP TABLE `user`;
CREATE TABLE `user` (
`id` int(12) NOT NULL AUTO_INCREMENT,
`username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
`email_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
`password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
`name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
`dob` DATE COLLATE utf8mb4_unicode_ci DEFAULT NULL,
`age` int(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
`gender` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT 'human' NOT NULL,
`nationality` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
`user_role` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT 'user' NOT NULL,
-- user | admin -- default = user
`status` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT 'active' NOT NULL,
-- active | inactive | deactivated -- default = active
`status_account` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT 'unlocked' NOT NULL, 
-- locked or unlocked -- default = unlocked
`status_disabled` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT 'no' NOT NULL,
-- yes or no or disable -- default = unlocked
-- status_disabled if account locked disable flag will be yes for a span of time for example 24hrs for a feature. example he can't make transaction.
`status_verification` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT 'unverified' NOT NULL,
-- verified | unverified -- default = unverified
`status_disabled_expiry` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '24' NOT NULL, -- 24hrs
`password_reset_url` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
`password_reset_code` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
`password_attempt_count` tinyint(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
`image_url` TEXT COLLATE utf8mb4_unicode_ci DEFAULT NULL,
`created_by` int(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
`created_date` TIMESTAMP COLLATE utf8mb4_unicode_ci DEFAULT CURRENT_TIMESTAMP NOT NULL,
`updated_by` int(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
`updated_date` TIMESTAMP COLLATE utf8mb4_unicode_ci DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
PRIMARY KEY (`id`),
UNIQUE KEY `username` (`username`),
UNIQUE KEY `email_address` (`email_address`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
