-- drop database `field_app_dev`;
create database `field_app_dev`;
use `field_app_dev`;

-- DROP TABLE `user`;
CREATE TABLE `user` (
  -- `id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` int(15) NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT '1',
  `type` tinyint(4) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_address` TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_tel` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cell_no` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `g_acount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expire` date NOT NULL,
  `password_reset` tinyint(4) NOT NULL DEFAULT '0',
  `log_count` tinyint(4) NOT NULL DEFAULT '0',
  `notes` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '9',
  `account_expire` date NOT NULL,
  `password_old` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_reset_date` date DEFAULT NULL,
  `login_attempt_stamp` datetime NOT NULL,
  `created_by` int(15) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` int(15) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `deleted_by` int(15) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  -- PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  UNIQUE KEY `mail_address` (`email_address`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- DROP TABLE `notification`;
CREATE TABLE `notification` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL, -- int(15) NOT NULL,
  `title` TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(15) DEFAULT NULL, -- NOT NULL,
  `created_date` datetime DEFAULT NULL, -- NOT NULL,
  `updated_by` int(15) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `deleted_by` int(15) DEFAULT NULL,
  `deleted_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
