const db = require("../../../models");
const User = db.user;
const Notification = db.notification;
const Op = db.Sequelize.Op;
const { ValidationError } = db.Sequelize;

const chalk = require('chalk');
const bcrypt = require("bcrypt");

// LOCAL FUNCTION DECLARE HERE
// User.beforeUpsert(async (user, options) => {
//   // console.log('beforeUpsert user options: ', options.instance.dataValues)
//   const hashedPassword = await _hashPassword(options.instance.dataValues.password);
//   options.instance.dataValues.password = hashedPassword;
//   user.password = hashedPassword;
//   // console.log('beforeUpsert user: ', user.password)
//   // console.log('beforeUpsert options: ', options)
//   // console.log('beforeUpsert user: ', user)
//   // console.log('beforeUpsert user: ', user.password)
// });
// User.afterUpsert(async (user, options) => {
//   console.log('afterUpsert user: ', user)
//   console.log('afterUpsert options: ', options)
//   // console.log('afterUpsert user: ', user[0])
//   // // const hashedPassword = await _hashPassword(user[0].password);
//   // // user[0].password = hashedPassword;
//   // user[0].dataValues.password = user[0].password;
//   // console.log('afterUpsert user: ', user[0].dataValues.password)
//   // console.log('afterUpsert user: ', user[0].password)
// });
// User.afterBulkUpdate(async (user, options) => {
//   console.log('afterBulkUpdate user: ', user)
// });
// User.afterCreate(async (user, options) => {
//   console.log('afterCreate user: ', user)
// });
// User.afterUpdate(async (user, options) => {
//   console.log('afterUpdate user: ', user)
// });
// User.afterSave(async (user, options) => {
//   console.log('afterSave user: ', user)
// });
// User.afterCommit(async (user, options) => {
//   console.log('afterCommit user: ', user)
// });
// User.beforeCreate(async (user, options) => {
//   // console.log(options)
//   const hashedPassword = await _hashPassword(user.password);
//   user.password = hashedPassword;
  
// });
// User.beforeSave(async (user, options) => {
//   console.log('beforeSave user: ', user)
//   // console.log('beforeSave options: ', options)
  
// });
// User.afterSave(async (user, options) => {
//   console.log('afterSave user: ', user)
// });
// User.afterCreate(async (user, options) => {
//   console.log('afterCreate user: ', user)
//   console.log('afterCreate options: ', options)
// });
// User.beforeBulkCreate(async (user, options) => {
//   console.log('beforeBulkCreate user: ', user)
//   console.log('beforeBulkCreate options: ', options)
// });
// User.afterBulkCreate(async (user, options) => {
//   console.log('afterBulkCreate user: ', user)
//   console.log('afterBulkCreate options: ', options)
// });
// User.beforeUpdate(async (user, options) => {
//   console.log('beforeUpdate user: ', user)
//   console.log('beforeUpdate user options: ', options)
// });
// User.afterUpsert(async (user, options) => {
//   console.log('afterUpsert user: ', user)
//   // console.log('afterUpsert user options: ', options)
// });
// User.beforeBulkUpdate(async (user, options) => {
//   console.log('beforeBulkUpdate user: ', user)
//   console.log('beforeBulkUpdate user options: ', options)
// });

const _hashPassword = async (userPass) => {
  // generate salt to hash password
  const salt = await bcrypt.genSalt(10);
  // now we set user password to hashed password
  return bcrypt.hash(userPass, salt);
};

const _lockAccount = async (data) => {
  return new Promise((resolve, reject) => {
    const condition = { status_account: 'locked' }
    console.info(condition)
    User.update(condition, { where: { email_address: data.email_address } })
      .then(count => {
        console.log('count: ', count)
        resolve(count);
      })
      .catch(err => {
        reject('something went wrong while updating your data.' + err);
      })
  })
};

const _validatePasswordCount = async (data, validatePassword) => {
  return new Promise((resolve, reject) => {
    const condition = validatePassword ? { password_attempt_count: 0 } : { password_attempt_count: (data.password_attempt_count ?? 0) + 1 }
    console.info('validatePasswordCount() condition: ', condition)
    User.update(condition, { where: { email_address: data.email_address} })
      .then(count => { //updated has a result of [ 1 ] we need to query again to get updated value
        const condition = data.email_address ? { where: { email_address: data.email_address } } : null;
        User.findOne(condition)
          .then(async updated_data => {
            resolve(updated_data.password_attempt_count)
          })
          .catch(err => {
            console.log(chalk.red('err:', err))
            reject('something went wrong while updating and fetching your data.' + err);
          });
      })
      .catch(err => {
        reject('something went wrong while updating your data.' + err);
      })
  })
};

module.exports = {
  authLoginUser: async (email_address, password) => {
    console.log(chalk.yellow("Function: authLoginUser"));
    console.log(chalk.blue(typeof email_address));
    console.log(chalk.blue(email_address));
    return new Promise((resolve, reject) => {

      // if (isNaN(Number(email_address))) {
      //   reject("Invalid user_id!");
      //   return;
      // }
      const condition = email_address ? { where: { email_address: email_address } } : null;
      User.findOne(condition)
      // User.findOne({ raw:true, where: { email_address: email_address } })
      // User.findByPk(id)
      .then(async data => {
        // console.log(chalk.green('data:', JSON.stringify(data)));
        console.log(chalk.green('data:', data));
        console.dir(data, { depth: null, colors:true });

        // PASSWORD ALGORITH HERE

        if (data) {
          const validatePassword = await bcrypt.compare(password, data.password);
          if (validatePassword) {
            if (data.status_account == 'locked') {
              reject('Your account has been lock due to excessive attempts. Please use the password reset service.');
            } else {
              await _validatePasswordCount(data, validatePassword);
              resolve(data)
            }
          } else {
            const validatePasswordCount = await _validatePasswordCount(data, validatePassword);
            if (+validatePasswordCount >= 5) {
              const lockAccount = await _lockAccount(data);
              reject('Your account has been lock due to excessive attempts. Please use the password reset service.');
            } else {
              reject('Password does not match the record we found. You only have ' + (5 - +validatePasswordCount) + ' tries left');
            }
            // res.status(400).json({ error: "Invalid Password" });
          }
        }else{
          reject("No result found for login data with email address of " + email_address);
        }
      })
      .catch(err => {
        console.log(chalk.red('err:' , err))
        reject(err);
      });

    });
  },


  createTestUser: async (req_body) => {
    return new Promise(async (resolve, reject) => {
      const user = {
        username: req_body.username,
        email_address:  req_body.email_address,
        password: req_body.password,
        name: req_body.name
      }

      user.password = await _hashPassword(user.password);

      User.create(user)
        .then(data => {
          // console.log(chalk.green('data:', data));
          // console.dir(data, { depth: null, colors: true });
          if (data) {
            // console.log(data)
            resolve(data);
          } else {
            reject("failed to insert table data on createTestUser()");
          }
        })
        .catch(err => {
          if (err instanceof ValidationError) {
            console.log(chalk.red('err instanceof ValidationError:', err.errors[0].message))
            reject('Captured validation error: ' + err.errors[0].message);
          }
          console.log(chalk.red('err:', err))
          reject(err);
        })
    })
  },

  createTestMultiUser: (req_body) => {
    return new Promise((resolve, reject) => {
      // generate salt to hash password
      const salt = bcrypt.genSalt(10);
      // now we set user password to hashed password
      // req_body.password = bcrypt.hash(user.password, salt);

      User.create(req_body)
        .then(data => {
          console.log(chalk.green('data:', data));
          console.dir(data, { depth: null, colors: true });
          if (data) {
            console.log('callback!');
            resolve(data);
          } else {
            reject("failed to insert table data on createTestUser()");
          }
        })
        .catch(err => {
          if (err instanceof ValidationError) {
            console.log(chalk.red('err instanceof ValidationError:', err.errors[0].message))
            reject('Captured validation error: ' + err.errors[0].message);
          }
          console.log(chalk.red('err:', err))
          reject(err);
        })
    })
  }
}
