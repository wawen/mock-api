const router = require('express').Router();
const UserController = require('./UserController');
const RouteConstant = require('../../../constant/Routes');
const Validation = require('../../../validation/UserValidation')

module.exports = (app) => {

  router.route('/login')
    .post(
      Validation.authLoginUser(),
      UserController.authLoginUser
    );

  router.route('/logout')
    .get(
      UserController.authLogoutUser
    );

  router.route('/create-test-user')
    .post(
      Validation.createTestUser(),
      UserController.createTestUser
    );

  router.route('/create-test-user-multi')
    .post(
      UserController.createTestMultiUser
    );

  router.route('/test-upload')
    .post(
      UserController.testUpload
    );
  
  router.route('/servicibility/check/:errorCode')
    .post(
      UserController.servicibilityCheck
    );

  router.route('/crif/:errorCode')
    .post(
      UserController.crif
    );

  app.use(
    RouteConstant.USER_PUBLIC,
    router
  );
};

// function setConnectionTimeout(time) {
//   var delay = typeof time === 'string'
//     ? ms(time)
//     : Number(time || 5000);

//   return function (req, res, next) {
//     res.connection.setTimeout(delay);
//     next();
//   }
// }
