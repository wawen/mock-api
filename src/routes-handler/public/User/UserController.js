// By default, the client will authenticate using the service account file
// specified by the GOOGLE_APPLICATION_CREDENTIALS environment variable and use
// the project specified by the GOOGLE_CLOUD_PROJECT environment variable. See
// https://github.com/GoogleCloudPlatform/google-cloud-node/blob/master/docs/authentication.md
// These environment variables are set automatically on Google App Engine
const { Storage } = require('@google-cloud/storage');
const config = require('config').get(mode);
const keysConfig = config.keys;
const corsConfig = config.cors;
const googleConfig = config.google;

let jwt = require('jsonwebtoken');
const { validationResult } = require('express-validator');

const UserServices = require('../_services/UserServices');
const reqResponse = require('../../../cors/responseHandler');
const getErrorStatus = require('../../../constant/ErrorData');

// REQUIRE FOR GOOGLE STORAGE UPLOAD
const cred = require(googleConfig.SERVICE_ACCOUNT);
const { format } = require('util');
const path = require('path');
const googleConfigStorage = {
	projectId: cred.project_id,
	keyFilename: path.join(__dirname, '..', '..', '..', '..', '/config/credentials/', googleConfig.KEYFILENAME),
};

// for csv 
const streamifier = require('streamifier');
const csv = require('csvtojson')
const iconv = require('iconv-lite')
const Encoding = require('encoding-japanese');
let fs = require('fs');

module.exports = {
	// Create Logout Function to Destroy the existing httpCookieOnly
	authLoginUser: async (req, res) => {
		try {
			const valErrors = validationResult(req);
			if (!valErrors.isEmpty()) {
				console.log(valErrors)
				if (valErrors.errors[0].value == undefined) {
					return res.status(402).send(reqResponse.errorResponse(402, valErrors));
				} else {
					return res.status(403).send(reqResponse.errorResponse(403, valErrors));
				}
			}
			// similar to 
			// email_address = req.body.email_address;
			// password = req.body.password;
			const { email_address, password } = req.body;
			console.log(email_address)
			console.log(password)

			let result = await UserServices.authLoginUser(email_address, password);
			// need to pass to another variable
			// to delete a key.
			var userData = result.dataValues;
			delete userData.password
			console.log(userData)

			// create a token
			var token = jwt.sign({ auth_data: userData }, corsConfig.secret, {
				expiresIn: 86400 // expires in 24 hours
			});

			auth_data = {
				// email_address: result.email_address,
				// name: result.name,
				// auth: true,
				// data: result,
				token: token
			}
			// if (pass_word) {
			//
			// }


			// Set the options for the cookie
			let cookieOptions = {
				// Delete the cookie after 90 days
				expires: new Date(Date.now() + 1 * 24 * 60 * 60 * 1000),
				// Set the cookie's HttpOnly flag to ensure the cookie is 
				// not accessible through JS, making it immune to XSS attacks
				sameSite: 'none',  
				httpOnly: true,
				// domain: 'sample site',
			};
			// In production, set the cookie's Secure flag 
			// to ensure the cookie is only sent over HTTPS
			if (process.env.NODE_ENV === 'production') {
				cookieOptions.secure = true;
			}else{
				cookieOptions.secure = false;
			}

			res.cookie('jwt', token, cookieOptions).status(201).send(reqResponse.successResponse(201, "Successfully gathered user data.", auth_data));
		} catch (error) {
			console.error('catchFromController: ', error)
			res.status(502).send(reqResponse.errorResponse(502, String(error)))
		}
	},

	authLogoutUser: async (req, res) => {
		try {
			res.clearCookie('jwt').status(201).send(reqResponse.successResponse(201, "Successfully gathered user data for logout.", "Successfull logout user."))
		} catch (error) {
			console.error('catchFromController authLogoutUser: ', error)
			res.status(502).send(reqResponse.errorResponse(502, String(error)))
		}
	},

	createTestUser: async (req, res) => {
		try {
			const valErrors = validationResult(req);
			if (!valErrors.isEmpty()) {
				console.log(valErrors)
				if (valErrors.errors[0].value == undefined) {
					return res.status(402).send(reqResponse.errorResponse(402, valErrors));
				} else {
					return res.status(403).send(reqResponse.errorResponse(403, valErrors));
				}
			}


			let result = await UserServices.createTestUser(req.body)

			res.status(201).send(reqResponse.successResponse(201, "Successfully created user data.", result))
		} catch (error) {
			console.error('catchFromController: ', error)
			// if (error.includes('username must be unique')) {
			// 	res.status(404).send(reqResponse.errorResponse(404, String(error)));
			// } else {
				res.status(502).send(reqResponse.errorResponse(502, String(error)));
			// }
		}
	},
	
	createTestMultiUser: async (req, res) => {
		try {
			const valErrors = validationResult(req);
			if (!valErrors.isEmpty()) {
				console.log(valErrors)
				if (valErrors.errors[0].value == undefined) {
					return res.status(402).send(reqResponse.errorResponse(402, valErrors));
				} else {
					return res.status(403).send(reqResponse.errorResponse(403, valErrors));
				}
			}
			
			let result = await UserServices.createTestMultiUser(req.body)

			res.status(201).send(reqResponse.successResponse(201, "Successfully created user data.", result))
		} catch (error) {
			console.error('catchFromController: ', error)
			// if (error.includes('username must be unique')) {
			// 	res.status(404).send(reqResponse.errorResponse(404, String(error)));
			// } else {
				res.status(502).send(reqResponse.errorResponse(502, String(error)));
			// }
		}
	},

	testUpload: async (req, res, next) => {
		try {
			var file = req.files.uploadedImages[0];
			console.log(file)
			console.log(req.files)
			// Instantiate a storage client
			// const storage = new Storage();
			// Creates a client from a Google service account key
			const storage = new Storage(googleConfigStorage);

			const bucket = storage.bucket(googleConfig.GOOGLE_STORAGE_BUCKET);
			if (!file) {
				res.status(400).send('No file uploaded.');
				return;
			}
			// FOR PUBLIC LINK MAKE SURE TO SET THE BUCKET SETTING IN
			// Disable Public Prevention Access
			// Create a new blob in the bucket and upload the file data.
			const blob = bucket.file(file.originalname);
			const blobStream = blob.createWriteStream();
			
			blobStream.on('error', err => {
				next(err);
			});
			
			blobStream.on('finish', async () => {
				// The public URL can be used to directly access the file via HTTP.
				const publicUrl = format(
					`https://storage.googleapis.com/${bucket.name}/${blob.name}`
					);
					result = {
						publicUrl: publicUrl,
						file: file
					}
					
					// Add below line for public url acess for files inside folder/bucket.
					await blob.makePublic();
					res.status(201).send(reqResponse.successResponse(201, "Successfully created user data.", result))
			});

			blobStream.end(file.buffer);
		} catch (error) {
			console.log(error)
			res.status(502).send(reqResponse.errorResponse(502, String(error)));
		}
	},
	//image upload only
	uploadImage() {
		try {
			var file = req.files[0];

			// Instantiate a storage client
			// const storage = new Storage();
			// Creates a client from a Google service account key
			const storage = new Storage(googleConfigStorage);

			const bucket = storage.bucket(googleConfig.GOOGLE_STORAGE_BUCKET);
			if (!file) {
				res.status(400).send('No file uploaded.');
				return;
			}

			// Create a new blob in the bucket and upload the file data.
			const blob = bucket.file(file.originalname);
			const blobStream = blob.createWriteStream();

			blobStream.on('error', err => {
				next(err);
			});

			blobStream.on('finish', () => {
				// The public URL can be used to directly access the file via HTTP.
				const publicUrl = format(
					`https://storage.googleapis.com/${bucket.name}/${blob.name}`
				);
				result = {
					publicUrl: publicUrl,
					file: file
				}
				res.status(201).send(reqResponse.successResponse(201, "Successfully created user data.", result))
			});

			blobStream.end(file.buffer);
		} catch (error) {
			console.log(error)
			res.status(502).send(reqResponse.errorResponse(502, String(error)));
		}
	},
	servicibilityCheck: async(req, res, next) => {
		try {
			const { errorCode } = req.params;
			let errorParams = {};

			// let findErrorCode = getErrorStatus.ERROR_STATUS_ARRAY.find(c => c.status == errorCode);
			// json file with the data
			var data = fs.readFileSync(path.resolve(__dirname, "../response-payload.txt"));

			var elements = JSON.parse(data);

			// results from result
			results = elements;
			if (errorCode != 200) {
				errorParams.code = errorCode;
				errorParams.description = "Something Went Wrong!";
				throw errorParams;
				// res.status(errorCode).send(reqResponse.errorResponse(errorCode, String("Something went wrong!cls")));
			}
			res.status(200).send(reqResponse.successResponse(200, "Successfully resolve response data.", results))
		} catch (error) {
			// console.error('catchFromController: ', error)
			res.status(error.code).send(reqResponse.errorResponse(error.code, String(error.description)));
		}
	},
	crif: async (req, res, next) => {
		try {
			const { errorCode } = req.params;
			let errorParams = {};

			// let findErrorCode = getErrorStatus.ERROR_STATUS_ARRAY.find(c => c.status == errorCode);
			// json file with the data
			var data = fs.readFileSync(path.resolve(__dirname, "../crif-payload.txt"));

			var elements = JSON.parse(data);

			// results from result
			results = elements;
			if (errorCode != 200) {
				errorParams.code = errorCode;
				errorParams.description = "Something Went Wrong!";
				throw errorParams;
				// res.status(errorCode).send(reqResponse.errorResponse(errorCode, String("Something went wrong!cls")));
			}
			res.status(200).send(reqResponse.successResponse(200, "Successfully resolve response data.", results))
		} catch (error) {
			// console.error('catchFromController: ', error)
			res.status(error.code).send(reqResponse.errorResponse(error.code, String(error.description)));
		}
	}
}
