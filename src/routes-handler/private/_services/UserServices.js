const db = require("../../../models");
const User = db.user;
const Progress = db.progress;
const Op = db.Sequelize.Op;
const { ValidationError } = db.Sequelize;

const chalk = require('chalk');
const async = require('async');
const bcrypt = require("bcrypt");

// for csv 
const streamifier = require('streamifier');
const csv = require('csvtojson')
const iconv = require('iconv-lite')
const Encoding = require('encoding-japanese');

// LOCAL FUNCTIONS
const funcCondition = (data, password, id) => {
  if (data) {
    return { password: password, created_by: id }
    // return { password: password, created_by: id, flag: "A" }
  } else {
    return { password: password, updated_by: id }
    // return { password: password, updated_by: id, flag: "C" }
  }
}

module.exports = {
  getSingleUser: async (user_id) => {
    return new Promise((resolve, reject) => {
      console.dir(User.primaryKeyAttributes);
      console.log(chalk.yellow("Function: getSingleUser"));
      console.log(chalk.blue(typeof user_id));
      console.log(chalk.blue(user_id));
      // if (isNaN(Number(user_id))) {
      //   reject("Invalid user_id!");
      //   return;
      // }
      var condition = user_id ? { where: { id: user_id } } : null;
      // User.findOne(condition)
      User.findOne({ raw:true, where: { id: user_id } })
      // User.findByPk(user_id)
        .then(data => {
          // console.log(chalk.green('data:', JSON.stringify(data)));
          console.log(chalk.green('data:', data));
          if (data) {
            resolve(data);
          }else{
            reject("No result found for data with user_id of " + user_id);
          }
        })
        .catch(err => {
          console.log(chalk.red('err:' , err))
          reject(err);
        });
    });
  },
  getAllUser: async (limit, offset, searchString) => {
    return new Promise((resolve, reject) => {
      console.log(chalk.yellow("Function: getAllUser"));
      const condition = searchString ? {
        [Op.or]: [
          {
            username: {
              [Op.like]: `%${searchString}%`
            }
          },
          {
            email_address: {
              [Op.like]: `%${searchString}%`
            }
          },
          {
            name: {
              [Op.like]: `%${searchString}%`
            }
          }
        ]
      } : null;
      console.log(condition)
      User.findAndCountAll({
        attributes: { exclude: ['password'] },
        where: condition,
        limit: limit,
        offset: offset 
      })
        .then(data => {
          console.log(chalk.green('data:', JSON.stringify(data)));
          console.log(chalk.green('data:', data));
          console.log(data)
          resolve(data);
        })
        .catch(err => {
          console.log(chalk.red('err:' , err))
          reject(err);
        });
    });
  },
  updateUser: async (id, req_body) => {
    return new Promise((resolve, reject) => {
      const condition = id ? { where: { id: id } } : null
      User.findByPk(id)
        .then(async data => {
          console.info(data);
          console.info(chalk.green('data:', data))
          console.log(chalk.green('data:', data.username))
          if (data.username != req_body.username && data.email_address != req_body.email_address){
            User.update(req_body, condition)
              .then(count => {
                User.findByPk(id)
                  .then(async updated_data => {
                    console.log("check if dirty model: ", updated_data.changed())
                    console.log(chalk.green('err:', updated_data))
                    resolve(updated_data)
                  })
                  .catch(err => {
                    if (err instanceof ValidationError) {
                      console.log(chalk.red('err instanceof ValidationError:', err.errors[0]))
                      console.error(err.errors[0])
                      reject('Captured validation error: ' + err.errors[0].message);
                    }
                    console.log(chalk.red('err:', err))
                    reject('something went wrong while updating and fetching your data.' + err);
                  });
              })
              .catch(err => {
                if (err instanceof ValidationError) {
                  console.log(chalk.red('err instanceof ValidationError:', err.errors[0]))
                  console.error(err.errors[0])
                  reject('Captured validation error: ' + err.errors[0].message);
                }
                console.error(err)
                reject('Catch: something went wrong while updating your data.' + err);
              })
          }else{
            // console.info(data);
            // console.info(req_body);
            for (const [key, value] of Object.entries(req_body)) {
              data[key] = value;
            }
            // console.info(data);
            // console.dir(data, { depth: null, colors: true });
            console.log("check if dirty model: ", data.changed())
            await data.save();
            console.log("check if dirty model: ", data.changed())
            resolve(data)
          }
        })
        .catch(err => {
          if (err instanceof ValidationError) {
            console.log(chalk.red('err instanceof ValidationError:', err.errors[0]))
            console.error(err.errors[0])
            reject('Captured validation error: ' + err.errors[0].message);
          }
          console.log(chalk.red('err:', err))
          reject('something went wrong while updating and fetching your data.' + err);
        });
    });
  },
  createUser: async (data, params, query) => {
    return new Promise((resolve, reject) => {
      // reject(true);
    });
  },

  // upsertCreate: (reqData) => {
  //   return new Promise((resolve, reject) => {
  //     User.bulkCreate(reqData, {
  //       ignoreDuplicates: true,
  //       returning: true
  //     })
  //     .then(data =>{
  //       console.log(data);
  //       resolve(data);
  //     })
  //     .catch(err => {
  //       if (err instanceof ValidationError) {
  //         console.log(err.errors[0])
  //         console.log(chalk.red('err instanceof ValidationError:', err.errors[0].message))
  //         reject('Captured validation error: ' + err.errors[0].message);
  //       }
  //       console.log(chalk.red('err:', err))
  //       reject(err);
  //     })
  //   })
  // },
  upsertCreate: async (fileBuffer, loginUserID) => {
    return new Promise(async (resolve, reject) => {
      var myObj = []
      var counter = 0;
      var progressID;
      // uncomment can be use to validate file
      // var codes = new Uint8Array(fileBuffer);
      // var charsetMatch = Encoding.detect(codes);

      const stream = streamifier.createReadStream(fileBuffer).pipe(iconv.decodeStream("Shift_JIS")); // encoding into SJIS
      
      await csv({
        ignoreEmpty: true,
        nullObject: null,
      })
      /**
      * @param {object} json - return the obj value
      * @param {number} lineNumber - return number index
      * note: you can also use promise inside it will take a while but you can make more algos inside.
      */
      .fromStream(stream).subscribe(async (json, lineNumber) => {
        // return new Promise((resolveStream, rejectStream) => {
          myObj.push(json);
          counter++;
          // console.log(counter)
        // }) // end promise
      }) // end subscribe
      console.log("TAPOS NA ANG myObj at counter.");

      // LAGAY NYO SA SEPARATE FUNCTION IF GUSTO NYO PERO DATA ANG RESOLVE TRUE LANG
      // DITO KAYO GAGAWA NG PROGRESS ID
      let createProgress = await Progress.create({})
      .then(data=>{
        console.log(data);
        resolve(data);
        progressID = data.dataValues.id
        return true;
      })
      .catch(err=>{
        console.log(err);
        reject(err);
        return false;
      })

      // let setAllFlagToNull = await module.exports.setAllFlagToNull();
      // console.log("return by createProgress: ", createProgress)
      // return;

      if (!createProgress) return;

      console.log("PERO NAG RUN PADIN DITO")
      // First, we start a transaction and save it into a variable
      const t1 = await db.sequelize.transaction();
      // const t2 = await db.sequelize.transaction();


      // resolve(setAllFlagToNull)
      // return
      async.forEachSeries(Object.keys(myObj), async (keyValue, cb) =>{
        // const salt = await bcrypt.genSalt(10);
        // const pw = await bcrypt.hash(myObj[keyValue].password, salt);
        try {
          let upsertData = await module.exports.upsertData(myObj[keyValue], keyValue, t1, myObj.length);
          // console.log("NATAPOS ANG ISANG PROMISE")
          let updateUpserts = await module.exports.updateUpserts(upsertData, keyValue, loginUserID, myObj.length, t1, progressID);

          // let resetProgress = await module.exports.resetProgress(progressID);
          
          // console.log("NATAPOS ANG PANGALAWANG PROMISE")
        } catch (error) {
          await t1.rollback()
          // await t2.rollback()
          reject(error);
        }

        }, async completed => {
        // console.log("HINDI NAG ANTAY");
        console.log(myObj.length);
        console.log(counter);
        await t1.commit();
        // await t2.commit();
        resolve();
      });
    })
  },

  setAllFlagToNull: async () => {
    return new Promise((resolve, reject) => {
      User.findAll({ where: null })
        .then((user) => {
          // Check if record exists in db
          console.log(user)
          if (user) {
            // user.name = "admins";
            // user.save();
          }
          resolve();
        })
        .catch(err => {
          reject(err)
        });
    })
  },

  upsertData: async (myObj, keyValue, t, myObjLength) => {
    return new Promise(async (resolve, reject) => {
      // NOTE DO THE HASING ON THE BACKGROUND NEXT TASK
      // const salt = await bcrypt.genSalt(10);
      // const pw = await bcrypt.hash(myObj.password, salt);
      // myObj.password = pw;
      
      // /**
      // * @param {options} transaction - create transaction for commit and rollback
      // */
      User.upsert(myObj, {
        transaction: t
      })
        /**
        * Success upsert.
        * @param {array} data - contains upsert user data & boolean if update or insert
        * @param {object} data[0] - Contains Returning Result of Successful update or insert
        * @param {boolean} data[1] - boolean if update or insert #true=Insert #false=Updated
        */
        .then(data => {
          // console.log('upserting: ', (Number(keyValue)+1), '-', myObjLength);
          resolve(data);
        })
        .catch(err => {
          console.log("Run DITO");
          if (err instanceof ValidationError) {
            console.log("Run A");
            console.log(err.errors[0]);
            console.log(chalk.red('err instanceof ValidationError:', err.errors[0].message));
            reject('Captured validation error: ' + err.errors[0].message + " on username: " + err.errors[0].instance.username + " on email: " + err.errors[0].instance.email_address + " Line: " + (keyValue + 2));
          } else {
            console.log("Run B");
            console.log(chalk.red('err:', err))
            reject(err);
          }
        }) // end user db operation
    })
  },

  updateUpserts: async (data, keyValue, loginUserID, myObjLength, t, progressID) => {
    return new Promise((resolve, reject) => {
      ////////////////////////////////////////////////////////////////////////////
      // THIS IF FOR UPDATING IF INSERTED OR UPDATED
      // ADD CREATED BY IF INSERT | ADD UPDATED BY IF UPDATE
      let condition = funcCondition(data[1], data[0].password, loginUserID)
      User.update(condition, {
        where: { username: data[0].username },
        transaction: t
      })
      .then(async updated_data => {
        // console.log('created by & updated by - modify: ', (Number(keyValue)+1), '-', myObjLength);
        let updateProgress = await module.exports.updateProgress(keyValue, myObjLength, progressID)
        resolve()
      })
      .catch(err => {
        reject('something went wrong while updating and fetching your data.' + err);
      });
      // END PUT CREATED BY OR UPDATE BY ON INSERTED OR UPDATED
      // -------------------------------------------------------------------------
    })
  },

  updateProgress: async(keyValue, myObjLength, progressID) => {
    return new Promise((resolve, reject) => {
      ////////////////////////////////////////////////////////////////////////
      // FOR PROGRESS BAR
      Progress.update({ progress: ((Number(keyValue)+1) / myObjLength) * 100 }, {
        where: { id: progressID }
      }).then(res => {
        // console.log('updating the progress: ', ((Number(keyValue)+1) / myObjLength) * 100);
        resolve();
      }).catch(err => {
        reject('something went wrong while updating and fetching your data.' + err);
      });
        // END PROGRESS BAR
        // -------------------------------------------------------------------
    })
  },

  resetProgress: async(progressID) => {
    return new Promise((resolve, reject) => {
      ////////////////////////////////////////////////////////////////////////
      // FOR PROGRESS BAR
      Progress.update({ progress: 0 }, {
        where: { id: progressID }
      }).then(res => {
        // console.log('updating the progress: ', ((Number(keyValue)+1) / myObjLength) * 100);
        resolve();
      }).catch(err => {
        reject('something went wrong while updating and fetching your data.' + err);
      });
        // END PROGRESS BAR
        // -------------------------------------------------------------------
    })
  },

  getProgress: async(id) => {
    return new Promise((resolve, reject) => {
      Progress.findByPk(id)
      .then(res => {
        console.log(res)
        resolve(res)
      }).catch(err => {
        reject(err)
      })
    })
  }
}
