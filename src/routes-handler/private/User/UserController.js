const UserServices = require('../_services/UserServices');
const reqResponse = require('../../../cors/responseHandler');
const { validationResult } = require('express-validator');

const chalk = require('chalk');

module.exports = {
	getSingleUser: async (req, res) => {
		try {
			const id = req.params.id;
			console.log(chalk.red(JSON.stringify(req.decoded)))
			let result = await UserServices.getSingleUser(id);
			res.status(201).send(reqResponse.successResponse(201, "Successfully gathered user master data.", result));
		} catch (error) {
			// console.error(error)
			res.status(502).send(reqResponse.errorResponse(502, String(error)))
		}
	},
	getAllUser: async (req, res) => {
		try {
			let page = parseInt(req.query.page);
			let limit = parseInt(req.query.size);
			let searchString = req.query.searchString ?? null;

			const offset = page ? page * limit : 0;

			console.log("offset = " + offset);

			let result = await UserServices.getAllUser(limit, offset, searchString);

			const totalPages = Math.ceil(result.count / limit);
			const result_pagination = {
				"totalCount": result.count,
				"totalPages": totalPages,
				"pageNumber": page,
				"pageSize": result.rows.length,
				"users": result.rows
			};

			console.log(totalPages);
			console.log(result_pagination)
			console.log(result)

			res.status(201).send(reqResponse.successResponse(201, "Successfully gathered user master data.", result_pagination));
		} catch (error) {
			console.error(error)
			res.status(502).send(reqResponse.errorResponse(502, String(error)))
		}
	},
	createUser: async (req, res) => {
		const valErrors = validationResult(req);
		if (!valErrors.isEmpty()) {
			return res.status(402).send(reqResponse.errorResponse(402, valErrors));
		}
		// let data = req.body; // Can be in JSON or Form Data
		// let params = req.params; // params is inlined with the url separated by "/id"
		// let query = req.query; // query string line "id=1&name=erwinvicarme"
		try {
			let result = await UserServices.createUser(data, params, query);
			res.status(201).send(reqResponse.successResponse(201, "User Created", "User has been created successfully"));
		} catch (error) {
			console.error(error);
			res.status(502).send(reqResponse.errorResponse(502, String(error)))
		}
	},
	updateUser: (req, res) => {
		const valErrors = validationResult(req);
		if (!valErrors.isEmpty()) {
			console.log(valErrors)
			if (valErrors.errors[0].value == undefined) {
				return res.status(402).send(reqResponse.errorResponse(402, valErrors));
			} else {
				return res.status(403).send(reqResponse.errorResponse(403, valErrors));
			}
		}
		
		let { id } = req.params;
		UserServices.updateUser(id, req.body)
			.then((result) => {
				res.status(201).send(reqResponse.successResponse(201, "User Updated", "User has been updated successfully"));
			})
			.catch((error) => {
				console.error(error);
				res.status(502).send(reqResponse.errorResponse(502, String(error)));
			})
	},
	upsertCreate: async (req, res, next) => {
		try {
			// console.log('From Controller: ', req.decoded)
			console.log('From Controller: ', next)
			// console.log('From Controller: ', req)
			// console.log('From Controller: ', res)
			console.log('From Controller: ', req.file)
			console.log('From Controller: ', req.files)
			// return
			let loginUserID = req.decoded.auth_data.id
			let file = req.files.uploadedFiles[0];
			// console.log(file)
			// return
			let fileBuffer = file.buffer;

			let result = await UserServices.upsertCreate(fileBuffer, loginUserID)
			console.log("RESULT FROM CONTROLLER: ", result)

			// results from result
			results = {
				progress: result.progress,
				id: result.id
			}

			res.status(201).send(reqResponse.successResponse(201, "Successfully resolve response data.", results))
		} catch (error) {
			console.error('catchFromController: ', error)
			res.status(502).send(reqResponse.errorResponse(502, String(error)));
		}
	},
	
	getProgress: async (req, res) => {
		try {
			const { id } = req.params;
			// console.log(id)
			// return;
			let getProgress = await UserServices.getProgress(id)

			console.log("fromController getProgress: ", getProgress)

			res.status(201).send(reqResponse.successResponse(201, "Successfully resolve response data.", getProgress))
		} catch (error) {
			console.error('catchFromController: ', error)
			res.status(502).send(reqResponse.errorResponse(502, String(error)));
		}
	}
}
