const router = require('express').Router();
const UserController = require('./UserController');
const RouteConstant = require('../../../constant/Routes');
const Middleware = require('../../../cors/middleware').checkToken;
const Validation = require('../../../validation/UserValidation')

module.exports = (app) => {
  router.route('')
    .get(
      UserController.getAllUser
    );

  router.route('/info/:id')
    .get(
      UserController.getSingleUser
    );

  router.route('/create')
    .post(
      Validation.create(),
      UserController.createUser
    );

  router.route('/update-user/:id')
    .put(
      Validation.updateUser(),
      UserController.updateUser
    );

  router.route('/upsert-create')
    .post(
      UserController.upsertCreate
    );

  router.route('/get-progress/:id')
    .get(
      UserController.getProgress
    );
    
  app.use(
    RouteConstant.USER_PRIVATE,
    Middleware,
    router
  );
};
