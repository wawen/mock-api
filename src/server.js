const express = require('express');
//add morgan in future, to log request on terminal
const chalk = require('chalk');
const cors = require('cors');
const app = express();
const pack = require('../package');
// const mysql = require('mysql');
const path = require('path');
// if NODE_ENV value not define then dev value will be assign
mode = process.env.NODE_ENV || 'dev';
// mode can be access anywhere in the project
const config = require('config').get(mode);
const multer = require('./cors/uploadMiddleware');

// Can be pass to cors(corsOptions)
// this is to validate the credentials if using http Cookies
// Check documentation for more json elements of CORS
var corsOptions = {
  credentials: true,
  origin: config.serverUrlWebUrlLink //"http://localhost:4200"
};

app.use(cors(corsOptions));

// NOTE!: QueryString are automatically handled by the API
// json and url encoded are included on express v4 and up
// parse requests of content-type - application/json
app.use(express.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({
  extended: false
}));
// parse multipart form data

app.use(multer.upload)

// use only when you want to see the metric related to express app
// app.use(require('express-status-monitor')());

require('./routes')(app);

// Not used, for uploading on local
const dir = path.join(__dirname, 'assets');
app.use('/upload', express.static(dir));

// This is to initialize the database using model.
const db = require("./models");
// This is to only sync into existing database and tables
db.sequelize.sync();
// Uncomment the code below to reinitialize the database and tables
// // drop the table if it already exists
// db.sequelize.sync({ force: true }).then(() => {
//   console.log("Drop and re-sync db.");
// });


const start = () => (
  app.listen(config.port, () => {
    console.log(chalk.yellow('.......................................'));
    console.log(chalk.green(config.name));
    console.log(chalk.green(`Port:\t\t${config.port}`));
    console.log(chalk.green(`Mode:\t\t${config.mode}`));
    console.log(chalk.green(`App version:\t${pack.version}`));
    console.log(chalk.green("database connection is established"));
    console.log(chalk.yellow('.......................................'));
  })
);

start();

// Uncomment below code if you want to use native mysql connection
// dbConnection = () => {

//   // When you try to connect database please comment bellow start method
//   start();

//   // MYSQL database connection start

//   // const databaseConfig = config.database;
//   // // con can be access anywhere in the project
//   // con = mysql.createPool(databaseConfig);
//   //
//   // con.getConnection((err) => {
//   //   if (err) {
//   //     //- The server close the connection.
//   //     if (err.code === "PROTOCOL_CONNECTION_LOST") {
//   //       console.error("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
//   //     }
//   //
//   //     //- Connection in closing
//   //     else if (err.code === "PROTOCOL_ENQUEUE_AFTER_QUIT") {
//   //       console.error("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
//   //     }
//   //
//   //     //- Fatal error : connection variable must be recreated
//   //     else if (err.code === "PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR") {
//   //       console.error("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
//   //     }
//   //
//   //     //- Error because a connection is already being established
//   //     else if (err.code === "PROTOCOL_ENQUEUE_HANDSHAKE_TWICE") {
//   //       console.error("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
//   //     }
//   //
//   //     //- Anything else
//   //     else {
//   //       console.error("/!\\ Cannot establish a connection with the database. /!\\ (" + err.code + ")");
//   //     }
//   //
//   //     setTimeout(dbConnection, 5000);
//   //   } else {
//   //     app.app.set('con', con);
//   //     start();
//   //   }
//   // });
// }

// dbConnection();
