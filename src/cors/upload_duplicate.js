// const Multer = require('multer');
// // Multer is required to process file uploads and make them available via
// // req.files.
// const multer = Multer({
//     storage: Multer.memoryStorage(),
//     limits: {
//         fileSize: 5 * 1024 * 1024, // no larger than 5mb, you can change as needed.
//     },
// }).single('myfile');
const util = require("util");
const multer = require('multer')
const { extname, resolve } = require('path')
const maxSize = 2 * 1024 * 1024;

const diskStorage = multer.diskStorage({
    destination: (req, file, done) => {
        if (!file) return done(new Error('Upload file error'), null)
        return done(null, resolve(process.cwd(), 'src/images'))
    },
    filename: (req, file, done) => {
        if (file) {
            const imagePattern = /(jpg|jpeg|png|gif|svg)/gi
            const mathExt = extname(file.originalname).replace('.', '')

            if (!imagePattern.test(mathExt)) {
                return new TypeError('File format is not valid')
            }

            req.file = file.originalname
            return done(null, file.originalname)
        }
    }
})

const fileUpload = multer({ storage: diskStorage, limits: { fileSize: maxSize } }).single("file");
// let uploadFileMiddleware = util.promisify(fileUpload);
// module.exports = uploadFileMiddleware;
module.exports = fileUpload;