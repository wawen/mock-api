const config = require('config').get(mode);
const corsConfig = config.cors;

let jwt = require('jsonwebtoken');
const reqResponse = require('./responseHandler');

const checkToken = (req, res, next) => {
  let httpHeader;
  let token;
  if (req.headers['cookie']) {
    httpHeader = req.headers['cookie']
    console.log('httpHeader: ', httpHeader)
    token = get_cookies(httpHeader).jwt;
  } else if (req.headers['x-access-token'] || req.headers['authorization']) {
    httpHeader = req.headers['x-access-token'] || req.headers['authorization'];
    token = httpHeader.replace('Bearer ', '');
  }
  console.log(token)
  if (token) {
    let key = corsConfig.secret
    jwt.verify(token, key, {
      ignoreExpiration: true
    }, (err, decoded) => {
      if (err) {
        console.log('err: ', err);
        console.log('key: ', key);
        console.log('token: ', token);
        return res.status(414).send(reqResponse.errorResponse(414));
      } else {
        console.log('key: ', key);
        console.log('decoded: ', decoded);
        if (decoded.auth_data.user_role == 'admin') {
          decoded.isAdminUser = true;
        } else {
          decoded.isAdminUser = false;
        }

        console.log('decoded: ', decoded);
        req.decoded = decoded;
        console.log('req.decoded: ', req.decoded);
        next();
      }
    });
  } else {
    return res.status(415).send(reqResponse.errorResponse(415));
  }
};

const get_cookies = (request) => {
  console.log('cookies: ', request)

  var list = {},
    rc = request;

  rc && rc.split(';').forEach(function (cookie) {
    var parts = cookie.split('=');
    list[parts.shift().trim()] = decodeURI(parts.join('='));
  });

  return list;
};

const getLoginUserId = () => {
  
}
module.exports = {
  checkToken,
  // publicAccess
}



// function publicAccess(req, res, next) {
//   next()
// }
